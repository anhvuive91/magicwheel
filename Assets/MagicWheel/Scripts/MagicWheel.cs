﻿using UnityEngine;
using System.Collections;

public class MagicWheel : MonoBehaviour
{
    // Góc quay mong muốn
    [SerializeField]
    private float _endAngle;

    // Transform của vòng quay
    [SerializeField]
    private RectTransform _wheel;

    // Tốc độ quay
    [SerializeField]
    private float _rotateSpeed;

    // Thời gian quay
    private float _time;
    private Coroutine _doRotate;

    void Update()
    {
        // Start vòng quay khi nhấn phím Space
        if (Input.GetKeyDown(KeyCode.Space))
        {
            // Gọi hàm quay
            DoRotate();
        }

        if (Input.touchCount >= 2)
        {
            DoRotate();
        }
    }

    void DoRotate()
    {
        if (_doRotate != null)
            StopCoroutine(_doRotate); // Dừng quay nếu đã có
        _doRotate = StartCoroutine("Rotate"); // Khởi động vòng quay
    }

    IEnumerator Rotate()
    {
        float angle = 0;
        _rotateSpeed = 0; // Reset speed rotate
        _time = 0; // Reset time
        while (true)
        {
            if (_time < 1) // Quay nhan dần trong khoảng 1 s
            {
                _rotateSpeed += Time.deltaTime * 30;
                _wheel.transform.eulerAngles += Vector3.back * _rotateSpeed;
                angle = _wheel.eulerAngles.z;
            }
            else if (_time < 3) // Quay đều từ 1-3s
            {
                _wheel.transform.eulerAngles += Vector3.back * _rotateSpeed;
            }
            else if (_time < 5) // Quay chậm dần kết thúc vòng quay
            {
                // Left dần về góc mong muốn
                if (angle > _endAngle)
                    _endAngle += 360;

                angle = Mathf.Lerp(angle, -_endAngle, .05f);
                _wheel.eulerAngles = new Vector3(0, 0, angle);
            }
            else
            {
                StopCoroutine(_doRotate);
            }
            _time += Time.deltaTime; // Đếm thời gian quay
            yield return new WaitForSeconds(Time.deltaTime);
        }
    }
}
